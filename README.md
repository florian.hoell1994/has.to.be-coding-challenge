<!-- ABOUT THE PROJECT -->
## Coding Challenge

This is a short documentation of how to insatall, run and test the application.
Suggestions on how to improve the API for the second challenge are also included at the end of this file.

<!-- GETTING STARTED -->
## Getting Started

This project was created with CodeIgniter4, XAMPP and PHPUnit in Windows10.

### Prerequisites

XAMPP - Apache Webserver

  ```sh
  Download the newest version from the official page: https://www.apachefriends.org/de/download.html
  ```

Postman

  ```sh
  Download the newest version from the official page: https://www.postman.com/downloads/
  You can also use another application for sending requests to the API-Endpoint
  ```

Composer

  ```sh
  Download the newest version from the official page: https://getcomposer.org/download/
  Important: When installing composer use this path for php -> "...\xampp\php\php.exe"
  ```

### Configuration of XAMPP Webserver

For running the application it is required to configure a few files.

1. Cange in directory "...\xampp\apache\conf\extra" and open httpd-vhosts.conf
2. Uncomment, delete or edit the first <VirtualHost *:80> block and replace it with following vhost:
   ```sh
    <VirtualHost *:80>
    	ServerAdmin webmaster@localhost
    	DocumentRoot "..\htdocs\cc-csms\public"
    	ServerName localhost
    	ServerAlias localhost
    	ErrorLog "logs/localhost-error.log"
    	CustomLog "logs/localhost-access.log" common
    </VirtualHost>
   ```
3. The "DocumentRoot" path to the public folder of the app is properly set but you can change it to your specific needs.

4. Save and exit.

5. Than change into directory "...\xampp\php" and open the php.ini file.

6. Go to the "Dynamic Extensions" section and enable the "intl" extension by deleting the semicolon.

7. Save and exit. 

8. Download and extract the cc-csms.zip folder 

9. Paste the extracted folder into the htdocs folder of xampp.



<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE -->
## Usage

Everything is now set up and you can start (as admin) the Apache-Server and MySQL Database (required for tests) via the xampp control panel.
### Hit endpoint with e.g. Postman
  ```sh
  http://localhost/api/rate
  ```

The input is strictly defined and must look like this:
 ```sh
  {
     "rate": { "energy": 0.3, "time": 2, "transaction": 1 },
     "cdr": { "meterStart": 1204307, "timestampStart": "2021-04-05T10:04:00Z", "meterStop": 1215230, "timestampStop": "2021-04-05T11:27:00Z" }
  }

  ```
You can vary the numbers or timestamps for testing purposes.

Calculated output with above example values:
 ```sh
  {
      "overall": 7.04
      "components": { "energy": 3.277, "time": 2.767, "transaction": 1 }
  }


  ```

### Run tests

1. Open a terminal and change to the project root folder "...\cc-csms\"

2. By typing ./vendor/bin/phpunit you can run the tests for this endpoint

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE -->
## Improvements to the API (Challenge 2)

1. Maybe it would be convinient to also get the customer data (from charging card) to be able to to assign billing to a customer (->database).

2. A additional parameter "hours" in cdr could be helpful to avoid issues with timestamp calculation.

3. The same I would suggest for the consumed energy -> parameter "consumed" in cdr.

<p align="right">(<a href="#top">back to top</a>)</p>